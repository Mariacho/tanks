﻿using UnityEngine;

namespace App.Scripts.Utilities.Extentions
{
    public static class TransformExtensions
    {
        public static void OriginTo(this Transform transform, Transform target)
        {
            transform.position = target.position;
            transform.rotation = target.rotation;
        }
        
        public static void RandomRotationY(this Transform transform, float offSet)
        {
            var rotation =  new Vector3(
                transform.rotation.eulerAngles.x,
                transform.rotation.eulerAngles.y + Random.Range(-offSet, offSet),
                transform.rotation.eulerAngles.z);
            transform.rotation = Quaternion.Euler(rotation);
        }
    
    }
}