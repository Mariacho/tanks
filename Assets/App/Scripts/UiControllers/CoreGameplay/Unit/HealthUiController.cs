﻿using App.Scripts.Gameplay.CoreGameplay.Unit;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UiControllers.CoreGameplay.Unit
{
    public class HealthUiController : MonoBehaviour
    {
        [SerializeField] private UnitStats _unitStats;
        [SerializeField] private Slider _slider;

        private void Start()
        {
            _slider.maxValue = _unitStats.MaxHp;
            _slider.value = _unitStats.Hp;
        }

        private void OnEnable()
        {
            _unitStats.OnChangeHp += ChangeHp;
        }

        private void OnDisable()
        {
            _unitStats.OnChangeHp -= ChangeHp;
        }

        private void ChangeHp(int hp)
        {
            _slider.value = hp;
        }
    }
}