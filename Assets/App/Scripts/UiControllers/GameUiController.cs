﻿using System;
using App.Scripts.Gameplay.CoreGameplay;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UiControllers
{
    public class GameUiController : MonoBehaviour
    {
        [SerializeField] private GameManager _gameManager;
        [SerializeField] private GameObject _menuPanel;
        [SerializeField] private Button _startButton;

        //В приложениях с более сложным Ui создаются отдельные
        //контроллеры для каждого окна.
        //Если Ui имеет сложные элементы, то создаются специальные
        //вьюшки для них
        
        private void Start()
        {
            ShowMenu();
        }

        private void OnEnable()
        {
            _gameManager.OnEndGame += ShowMenu;
            _startButton.onClick.AddListener(StartClicked);
        }

        private void OnDisable()
        {
            _gameManager.OnEndGame -= ShowMenu;
            _startButton.onClick.RemoveListener(StartClicked);
        }

        private void StartClicked()
        {
            _menuPanel.SetActive(false);
            _gameManager.StartGame();
        }

        private void ShowMenu()
        {
            _menuPanel.SetActive(true);
        }
    }
}