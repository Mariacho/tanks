﻿using System.Collections;
using App.Scripts.Gameplay.CoreGameplay.Combat.Weapons;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Combat
{
    public class AttackController : MonoBehaviour
    {
        [field: SerializeField] public BaseWeapon CurrentWeapon { get; private set; }
        public AttackState CurrentState { get; private set; } = AttackState.Ready;
        [SerializeField] private Transform _firePosition;

        private void Start()
        {
            if (CurrentWeapon != null)
            {
                ChangeWeapon(CurrentWeapon, true);
            }
        }

        public enum AttackState
        {
            Ready,
            Reload
        }

        public void ChangeWeapon(BaseWeapon weapon, bool force = false)
        {
            if (CurrentState != AttackState.Reload || force)
            {
                StopAllCoroutines();
                CurrentWeapon = weapon;
                CurrentWeapon.Initialization(_firePosition);
            }
        }

        public void Fire()
        {
            if (CurrentState != AttackState.Reload)
            {
                StartCoroutine(FireLoop());
            }
        }

        private IEnumerator FireLoop()
        {
            CurrentState = AttackState.Reload;
            CurrentWeapon.Attack();
            yield return new WaitForSeconds(CurrentWeapon.FireRate);
            CurrentState = AttackState.Ready;
        }
    }
}