﻿using System;
using System.Collections.Generic;
using App.Scripts.Gameplay.CoreGameplay.Combat.Weapons;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Combat
{
    public class WeaponSelector : MonoBehaviour
    {
        [SerializeField] private AttackController _attackController;
        [SerializeField] private List<BaseWeapon> Weapons = new List<BaseWeapon>();
        private int _selectedWeaponIndex;

        public event Action<BaseWeapon> OnSelectWeapon;
        
        private void Start()
        {
            _selectedWeaponIndex = Weapons.FindIndex(item => item == _attackController.CurrentWeapon);
        }

        public void Next()
        {
            if (_attackController.CurrentState == AttackController.AttackState.Reload)
                return;
            
            _selectedWeaponIndex++;
            if (_selectedWeaponIndex >= Weapons.Count)
            {
                _selectedWeaponIndex = 0;
            }
            _attackController.ChangeWeapon(Weapons[_selectedWeaponIndex]);
            OnSelectWeapon?.Invoke(Weapons[_selectedWeaponIndex]);
        }

        public void Previous()
        {
            if (_attackController.CurrentState == AttackController.AttackState.Reload)
                return;
            
            _selectedWeaponIndex--;
            if (_selectedWeaponIndex < 0)
            {
                _selectedWeaponIndex = Weapons.Count - 1;
            }
            _attackController.ChangeWeapon(Weapons[_selectedWeaponIndex]);
            OnSelectWeapon?.Invoke(Weapons[_selectedWeaponIndex]);
        }
    }
}