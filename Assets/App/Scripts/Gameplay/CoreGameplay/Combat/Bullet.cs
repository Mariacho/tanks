﻿using System;
using System.Collections;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Combat
{
    public delegate void BulletHandler(Bullet sender);
    public delegate void BulletCollideHandler(Bullet sender, Collider collider);
    
    public class Bullet : MonoBehaviour
    {
        public event BulletCollideHandler OnCollided;
        public event BulletHandler OnLifeCycleEnd;
        [SerializeField] private Rigidbody _rigidbody;
        private float _lifeTime;
        private IEnumerator _lifeCycle;
        
        private void OnCollisionEnter(Collision other)
        {
            OnCollided?.Invoke(this, other.collider);
        }

        public void Initialization(float force, float lifeTime)
        {
            AddForce(force);
            SetLifeTime(lifeTime);
        }

        public void AddForce(float force)
        {
            _rigidbody.velocity = transform.forward * force;
        }

        public void SetLifeTime(float lifeTime)
        {
            _lifeTime = lifeTime;
            if (_lifeCycle != null)
            {
                StopCoroutine(_lifeCycle);
                _lifeCycle = null;
            }

            _lifeCycle = LifeCycle();
            StartCoroutine(_lifeCycle);
        }

        private IEnumerator LifeCycle()
        {
            yield return new WaitForSeconds(_lifeTime);
            OnLifeCycleEnd?.Invoke(this);
        }
    }
}