﻿using App.Scripts.Gameplay.CoreGameplay.Unit;

namespace App.Scripts.Gameplay.CoreGameplay.Combat.Static
{
    public static class DamageCalculator
    {
        public static int CalculateWithDefence(UnitStats stats, int damage)
        {
            return (int) (damage - damage * stats.Defence);
        }
    }
}