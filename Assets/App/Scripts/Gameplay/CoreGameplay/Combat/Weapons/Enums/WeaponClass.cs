﻿namespace App.Scripts.Gameplay.CoreGameplay.Combat.Weapons.Enums
{
    public enum WeaponClass
    {
        Bullet,
        Raycast
    }
}