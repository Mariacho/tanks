﻿namespace App.Scripts.Gameplay.CoreGameplay.Combat.Weapons.Enums
{
    public enum WeaponType
    {
        Rifle,
        Shotgun,
        Launcher
    }
}