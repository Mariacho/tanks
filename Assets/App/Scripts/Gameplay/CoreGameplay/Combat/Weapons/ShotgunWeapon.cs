﻿using App.Scripts.Common;
using App.Scripts.Gameplay.CoreGameplay.Combat.Static;
using App.Scripts.Gameplay.CoreGameplay.Combat.Weapons.Enums;
using App.Scripts.Gameplay.CoreGameplay.Unit;
using App.Scripts.Utilities.Extentions;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Combat.Weapons
{
    [CreateAssetMenu(fileName = "ShotgunWeapon", menuName = "Tanks/Weapon/Shotgun", order = 0)]
    public class ShotgunWeapon : BaseWeapon
    {
        public override WeaponType Type { get => WeaponType.Shotgun; }
        public override WeaponClass WeaponClass { get => WeaponClass.Bullet; }

        [Range(2, 10)] public int CountProjectiles = 3;
        [Range(5, 30)] public float EurlerOffset = 10;
        public float Force;
        public float LifeTime;
        private int PoolSize = 16;
        
        [Header("Визуал")] 
        public Bullet Bullet;
        public ParticleSystem ParticleCollide;

        public PoolObject<Bullet> BulletsPool;
        private GameObject _poolParant;

        public override void Initialization(Transform firePosition)
        {
            base.Initialization(firePosition);
            if (BulletsPool == null)
            {
                _poolParant = new GameObject();
                _poolParant.name = this.name + "_Pool";
                _poolParant.transform.SetParent(_firePosition);
                _poolParant.transform.OriginTo(_firePosition);
                BulletsPool = new PoolObject<Bullet>(Bullet, PoolSize, _poolParant.transform, true);
            }
        }

        public override void Attack()
        {
            for (int i = 0; i < CountProjectiles; i++)
            {
                var bullet = BulletsPool.GetObject();
                bullet.transform.SetParent(null);
                bullet.transform.OriginTo(_firePosition);
                bullet.transform.RandomRotationY(EurlerOffset);
                bullet.Initialization(Force, LifeTime);
                bullet.OnCollided += CheckCollide;
                bullet.OnLifeCycleEnd += DestroyBullet;
            }
        }

        private void CheckCollide(Bullet sender, Collider collider)
        {
            if (collider.TryGetComponent(out UnitStats stats))
            {
                stats.TakeDamage(DamageCalculator.CalculateWithDefence(stats, Damage));
                DestroyBullet(sender);
            }
        }

        private void DestroyBullet(Bullet sender)
        {
            sender.OnLifeCycleEnd -= DestroyBullet;
            sender.OnCollided -= CheckCollide;
            if (_poolParant != null)
            {
                sender.transform.SetParent(_poolParant.transform);
                BulletsPool.ReturnObject(sender);
            }
            else
            {
                Destroy(sender.gameObject);
            }
            //add effect
        }
    }
}