﻿using System;
using App.Scripts.Gameplay.CoreGameplay.Combat.Weapons.Enums;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Combat.Weapons
{
    public abstract class BaseWeapon : ScriptableObject
    {
        public virtual WeaponType Type { get; }
        public virtual WeaponClass WeaponClass { get; }

        public int Damage;
        public float FireRate;
        protected Transform _firePosition;

        public virtual void Attack()
        {
        }

        public virtual void Initialization(Transform firePosition)
        {
            _firePosition = firePosition;
        }

    }
}