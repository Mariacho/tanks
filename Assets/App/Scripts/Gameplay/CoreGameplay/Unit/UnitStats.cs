﻿using System;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Unit
{
    public sealed class UnitStats : MonoBehaviour
    {
        public int MaxHp => _maxHp;
        public int Hp { get; private set; }
        [field:SerializeField, Range(0, 1)] public float Defence { get; private set; } = 0;
        [field:SerializeField] public float MoveSpeed { get; private set; } = 0;
        [field:SerializeField] public float TurnSpeed { get; private set; } = 0;

        public event Action<UnitStats> OnDeath;
        public event Action<int> OnChangeHp;

        [SerializeField] private int _maxHp = 100;

        private void Awake()
        {
            Hp = MaxHp;
        }
        
        private void ChangeHp(int value)
        {
            Hp = Mathf.Clamp(value, 0, MaxHp);
            OnChangeHp?.Invoke(Hp);
            if (Hp == 0)
            {
                OnDeath?.Invoke(this);
            }
        }

        public void TakeDamage(int value)
        {
            ChangeHp(Hp - value);
        }

        public void TakeHeal(int value)
        {
            ChangeHp(Hp + value);
        }

        //todo: добавить возможность изменять скорость (сделать универсальный подход в плане характеристик)
    }
}