﻿using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Unit.Player
{
    public sealed class PlayerMover : MonoBehaviour
    {
        [SerializeField] private UnitStats _stats;
        [SerializeField] private PlayerInput _playerInput;
        [SerializeField] private Rigidbody _rigidbody;
        
        private void Update()
        {
            CalculateMovement(_playerInput.MoveDirection);
        }

        private void CalculateMovement(Vector2 direction)
        {
            Move(direction.y);
            Turn(direction.x);
        }

        private void Move(float direction)
        {
            var moveDirection = transform.forward * direction * _stats.MoveSpeed * Time.deltaTime;
            _rigidbody.MovePosition(_rigidbody.position + moveDirection);
        }

        private void Turn(float direction)
        {
            var turn = direction * _stats.TurnSpeed * Time.deltaTime;
            Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);
            _rigidbody.MoveRotation (_rigidbody.rotation * turnRotation);
        }

    }
}