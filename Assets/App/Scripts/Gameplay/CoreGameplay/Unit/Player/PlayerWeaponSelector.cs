﻿using System;
using App.Scripts.Gameplay.CoreGameplay.Combat;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Unit.Player
{
    public class PlayerWeaponSelector : MonoBehaviour
    {
        [SerializeField] private WeaponSelector _selector;
        [SerializeField] private PlayerInput _playerInput;

        private void OnEnable()
        {
            _playerInput.OnNextWeapon += NextWeapon;
            _playerInput.OnPreviousWeapon += PreviousWeapon;
        }

        private void NextWeapon()
        {
            _selector.Next();
        }

        private void PreviousWeapon()
        {
           _selector.Previous();
        }
    }
}