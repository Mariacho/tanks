﻿using System;
using App.Scripts.Gameplay.CoreGameplay.Unit.Player.Input;
using UnityEngine;
using UnityEngine.InputSystem;

namespace App.Scripts.Gameplay.CoreGameplay.Unit.Player
{
    public sealed class PlayerInput : MonoBehaviour
    {
        private PlayerActionsConfiguration _actionsConfiguration;

        public bool IsFirePressed { get; private set; }
        public event Action OnFire;
        public event Action OnNextWeapon;
        public event Action OnPreviousWeapon;
        public Vector2 MoveDirection => _actionsConfiguration.Main.Move.ReadValue<Vector2>();

        private Vector2 _lastMovePosition;
        
        private void Awake()
        {
            _actionsConfiguration = new PlayerActionsConfiguration();
        }

        private void OnEnable()
        {
            _actionsConfiguration.Main.Fire.performed += FireInvoke;
            _actionsConfiguration.Main.Fire.canceled += FireCancel;
            _actionsConfiguration.Main.WeaponNext.performed += WeaponNextInvoke;
            _actionsConfiguration.Main.WeaponLast.performed += WeaponLastInvoke;
            _actionsConfiguration.Enable();
        }

        private void OnDisable()
        {
            _actionsConfiguration.Main.Fire.performed -= FireInvoke;
            _actionsConfiguration.Main.Fire.canceled -= FireCancel;
            _actionsConfiguration.Main.WeaponNext.performed -= WeaponNextInvoke;
            _actionsConfiguration.Main.WeaponLast.performed -= WeaponLastInvoke;
            _actionsConfiguration.Disable();
        }

        private void FireCancel(InputAction.CallbackContext obj)
        {
            IsFirePressed = false;
        }

        private void FireInvoke(InputAction.CallbackContext obj)
        {
            IsFirePressed = true;
            OnFire?.Invoke();
        }

        private void WeaponNextInvoke(InputAction.CallbackContext obj)
        {
            OnNextWeapon?.Invoke();
        }

        private void WeaponLastInvoke(InputAction.CallbackContext obj)
        {
            OnPreviousWeapon?.Invoke();
        }
    }

}