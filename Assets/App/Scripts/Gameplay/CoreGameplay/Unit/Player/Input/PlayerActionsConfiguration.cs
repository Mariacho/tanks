// GENERATED AUTOMATICALLY FROM 'Assets/App/Settings/Input/PlayerActionsConfiguration.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace App.Scripts.Gameplay.CoreGameplay.Unit.Player.Input
{
    public class @PlayerActionsConfiguration : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @PlayerActionsConfiguration()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerActionsConfiguration"",
    ""maps"": [
        {
            ""name"": ""Main"",
            ""id"": ""19297175-b397-48b7-8c45-10403e74a360"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""d8a017aa-2ea6-424b-9a66-5b192420afd4"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""53211caa-b6f0-42c9-80aa-0d1480b42648"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""WeaponNext"",
                    ""type"": ""Button"",
                    ""id"": ""d71e0e02-fb6b-4699-96be-cefdf1f861d5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""WeaponLast"",
                    ""type"": ""Button"",
                    ""id"": ""81e96fc9-4da6-49cb-8914-00fcef6c2163"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""eaee42cf-5cd4-4b3f-8b01-af4801109b2f"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""7846e8c2-203e-4eed-9adb-641f6f10344f"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""1544c54f-21a5-41bd-8bba-8c52b011a8cc"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""871ef209-a34c-4255-8b88-c2d19115ea8a"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""85d03d77-dbc1-4cb5-998c-6b1fa2b3980a"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""84354a95-e75e-4155-ad4b-1efc915a25d9"",
                    ""path"": ""<Keyboard>/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a0b243af-eace-4263-85ca-b0f270490013"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""WeaponNext"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1e680394-af5a-4634-bf94-3dcd83117c73"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""WeaponLast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Main
            m_Main = asset.FindActionMap("Main", throwIfNotFound: true);
            m_Main_Move = m_Main.FindAction("Move", throwIfNotFound: true);
            m_Main_Fire = m_Main.FindAction("Fire", throwIfNotFound: true);
            m_Main_WeaponNext = m_Main.FindAction("WeaponNext", throwIfNotFound: true);
            m_Main_WeaponLast = m_Main.FindAction("WeaponLast", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Main
        private readonly InputActionMap m_Main;
        private IMainActions m_MainActionsCallbackInterface;
        private readonly InputAction m_Main_Move;
        private readonly InputAction m_Main_Fire;
        private readonly InputAction m_Main_WeaponNext;
        private readonly InputAction m_Main_WeaponLast;
        public struct MainActions
        {
            private @PlayerActionsConfiguration m_Wrapper;
            public MainActions(@PlayerActionsConfiguration wrapper) { m_Wrapper = wrapper; }
            public InputAction @Move => m_Wrapper.m_Main_Move;
            public InputAction @Fire => m_Wrapper.m_Main_Fire;
            public InputAction @WeaponNext => m_Wrapper.m_Main_WeaponNext;
            public InputAction @WeaponLast => m_Wrapper.m_Main_WeaponLast;
            public InputActionMap Get() { return m_Wrapper.m_Main; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(MainActions set) { return set.Get(); }
            public void SetCallbacks(IMainActions instance)
            {
                if (m_Wrapper.m_MainActionsCallbackInterface != null)
                {
                    @Move.started -= m_Wrapper.m_MainActionsCallbackInterface.OnMove;
                    @Move.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnMove;
                    @Move.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnMove;
                    @Fire.started -= m_Wrapper.m_MainActionsCallbackInterface.OnFire;
                    @Fire.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnFire;
                    @Fire.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnFire;
                    @WeaponNext.started -= m_Wrapper.m_MainActionsCallbackInterface.OnWeaponNext;
                    @WeaponNext.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnWeaponNext;
                    @WeaponNext.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnWeaponNext;
                    @WeaponLast.started -= m_Wrapper.m_MainActionsCallbackInterface.OnWeaponLast;
                    @WeaponLast.performed -= m_Wrapper.m_MainActionsCallbackInterface.OnWeaponLast;
                    @WeaponLast.canceled -= m_Wrapper.m_MainActionsCallbackInterface.OnWeaponLast;
                }
                m_Wrapper.m_MainActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Move.started += instance.OnMove;
                    @Move.performed += instance.OnMove;
                    @Move.canceled += instance.OnMove;
                    @Fire.started += instance.OnFire;
                    @Fire.performed += instance.OnFire;
                    @Fire.canceled += instance.OnFire;
                    @WeaponNext.started += instance.OnWeaponNext;
                    @WeaponNext.performed += instance.OnWeaponNext;
                    @WeaponNext.canceled += instance.OnWeaponNext;
                    @WeaponLast.started += instance.OnWeaponLast;
                    @WeaponLast.performed += instance.OnWeaponLast;
                    @WeaponLast.canceled += instance.OnWeaponLast;
                }
            }
        }
        public MainActions @Main => new MainActions(this);
        public interface IMainActions
        {
            void OnMove(InputAction.CallbackContext context);
            void OnFire(InputAction.CallbackContext context);
            void OnWeaponNext(InputAction.CallbackContext context);
            void OnWeaponLast(InputAction.CallbackContext context);
        }
    }
}
