﻿using System;
using App.Scripts.Gameplay.CoreGameplay.Combat;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Unit.Player
{
    public sealed class PlayerAttack : MonoBehaviour
    {
        [SerializeField] private PlayerInput _playerInput;
        [SerializeField] private AttackController _attackController;

        private void Update()
        {
            if (_playerInput.IsFirePressed)
            {
                _attackController.Fire();
            }
        }
    }
}