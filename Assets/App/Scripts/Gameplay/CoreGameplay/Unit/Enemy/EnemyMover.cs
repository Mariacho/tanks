﻿using UnityEngine;
using UnityEngine.AI;

namespace App.Scripts.Gameplay.CoreGameplay.Unit.Enemy
{
    public class EnemyMover : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent _navMeshAgent;
        [SerializeField] private UnitStats _stats;

        private void Start()
        {
            _navMeshAgent.speed = _stats.MoveSpeed;
            _navMeshAgent.angularSpeed = _stats.TurnSpeed;
        }
    }
}