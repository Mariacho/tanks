﻿using System;
using System.Collections;
using App.Scripts.Gameplay.CoreGameplay.Combat;
using App.Scripts.Gameplay.CoreGameplay.Radar;
using UnityEngine;
using UnityEngine.AI;

namespace App.Scripts.Gameplay.CoreGameplay.Unit.Enemy
{
    public class EnemyController : MonoBehaviour
    {
        //Предпочтительнее эту логику сделать стейт машиной с отдельными стейтами:
        //Follow, Attack, Death и т.п.

        public UnitStats Stats => _stats;
        [SerializeField] private UnitStats _stats;
        [SerializeField] private NavMeshAgent _navigation;
        [SerializeField] private AttackController _attackController;
        
        [SerializeField] private TargetsRadar _visionRadar;
        //В данном примере сделал через радар, но в крупных проектах
        //в вепоне должно быть поле дистанции срельбы и радиус
        //радара тогда бы определялся дистанцией стрельбы
        [SerializeField] private TargetsRadar _attackRadar;
        private Transform _target;
        
        private void OnEnable()
        {
            StartCoroutine(EnemyActionsLoop());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        private IEnumerator EnemyActionsLoop()
        {
            while (true)
            {
                yield return StartCoroutine(FindTarget());
                yield return StartCoroutine(FollowTarget());
                yield return StartCoroutine(AttackTarget());
            }
        }

        private IEnumerator FindTarget()
        {
            yield return new WaitWhile(() => _visionRadar.GetNearestTarget() == null);
            _target = _visionRadar.GetNearestTarget().transform;
        }

        private IEnumerator FollowTarget()
        {
            while (_attackRadar.GetNearestTarget() == null)
            {
                if (_target == null)
                {
                    yield break;
                }
                _navigation.SetDestination(_target.position);
                yield return new WaitForSeconds(0.1f);
            }
        }

        private IEnumerator AttackTarget()
        {
            while (_attackRadar.GetNearestTarget() != null)
            {
                _attackController.Fire();
                yield return new WaitForSeconds(0.1f);
            }
        }

    }
}