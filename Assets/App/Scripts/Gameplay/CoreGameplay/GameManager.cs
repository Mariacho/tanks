﻿using System;
using App.Scripts.Gameplay.CoreGameplay.Spawner;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay
{
    public class GameManager : MonoBehaviour
    {
        public event Action OnStartGame;
        public event Action OnEndGame;
        
        [SerializeField] private EnemySpawner _enemySpawner;
        [SerializeField] private PlayerSpawner _playerSpawner;
        
        public void StartGame()
        {
            _playerSpawner.SpawnPlayer();
            _enemySpawner.SpawnEnemies();
            OnStartGame?.Invoke();
        }

        public void EndGame()
        {
            _playerSpawner.DespawnPlayer();
            _enemySpawner.DespawnAllEnemies();
            OnEndGame?.Invoke();
        }

        private void EndGameInvoke()
        {
            EndGame();
        }

        private void OnEnable()
        {
            _playerSpawner.OnPlayerDeath += EndGameInvoke;
        }

        private void OnDisable()
        {
            _playerSpawner.OnPlayerDeath -= EndGameInvoke;
        }
    }
}