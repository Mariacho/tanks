﻿using System;
using System.Collections.Generic;
using App.Scripts.Gameplay.CoreGameplay.Combat;
using App.Scripts.Gameplay.CoreGameplay.Combat.Weapons;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Customization
{
    public sealed class WeaponCustomizationSystem : MonoBehaviour
    {
        [SerializeField] private Transform _customizationSlot;
        [SerializeField] private WeaponSelector _selector;
        [SerializeField] private List<WeaponCustomize> CustomizePresets = new List<WeaponCustomize>();
        
        private List<WeaponCustomize> _presets = new List<WeaponCustomize>();
        
        
        [Serializable]
        public class WeaponCustomize
        {
            public BaseWeapon Weapon;
            public GameObject VisualElements;

            public WeaponCustomize(BaseWeapon weapon, GameObject visualElements)
            {
                Weapon = weapon;
                VisualElements = visualElements;
            }
        }

        private void Awake()
        {
            foreach (var preset in CustomizePresets)
            {
                var visualElement = Instantiate(preset.VisualElements, _customizationSlot);
                _presets.Add(new WeaponCustomize(preset.Weapon, visualElement));
            }
        }

        private void OnEnable()
        {
            _selector.OnSelectWeapon += ChangeCustomize;
        }

        private void OnDisable()
        {
            _selector.OnSelectWeapon -= ChangeCustomize;
        }

        private void ChangeCustomize(BaseWeapon weapon)
        {
            HideAll();
            Show(weapon);
        }

        private void HideAll()
        {
            foreach (var preset in _presets)
            {
                preset.VisualElements.SetActive(false);
            }
        }

        private void Show(BaseWeapon weapon)
        {
            var index = _presets.FindIndex(item => item.Weapon == weapon);
            _presets[index].VisualElements.SetActive(true);
        }
        
    }
}