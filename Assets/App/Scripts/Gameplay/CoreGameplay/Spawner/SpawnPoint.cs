﻿using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Spawner
{
    public class SpawnPoint : MonoBehaviour
    {
#if UNITY_EDITOR
        public Color ColorGizmos;
        private float _radius = 1;
        
        private void OnDrawGizmos()
        {
            Gizmos.color = ColorGizmos;
            Gizmos.DrawSphere(new Vector3(
                transform.position.x,
                transform.position.y + _radius,
                transform.position.z), _radius);
        }
#endif
        
    }
}