﻿using System;
using App.Scripts.Gameplay.CoreGameplay.Unit;
using Cinemachine;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Spawner
{
    public class PlayerSpawner : MonoBehaviour
    {
        public event Action OnPlayerDeath;
        
        [SerializeField] private SpawnPoint _point;
        [SerializeField] private UnitStats _playerPrefab;
        [SerializeField] private CinemachineVirtualCamera _camera;
        public UnitStats PlayerInstance { get; private set; }

        public void SpawnPlayer()
        {
            PlayerInstance = Instantiate(_playerPrefab, _point.transform);
            PlayerInstance.transform.SetParent(this.transform);
            _camera.Follow = PlayerInstance.transform;
            PlayerInstance.OnDeath += PlayerDeath;
        }
        
        public void DespawnPlayer()
        {
            if (PlayerInstance != null)
            {
                PlayerInstance.OnDeath -= PlayerDeath;
                Destroy(PlayerInstance.gameObject);
            }
        }
        
        private void PlayerDeath(UnitStats obj)
        {
            OnPlayerDeath?.Invoke();
        }

    }
}