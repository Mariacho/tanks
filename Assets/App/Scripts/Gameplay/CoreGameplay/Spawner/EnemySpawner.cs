﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Scripts.Gameplay.CoreGameplay.Unit;
using App.Scripts.Gameplay.CoreGameplay.Unit.Enemy;
using UnityEngine;
using Random = UnityEngine.Random;

namespace App.Scripts.Gameplay.CoreGameplay.Spawner
{
    public class EnemySpawner : MonoBehaviour
    {
        public event Action OnDespawnEnemy; 
        
        [SerializeField] private List<SpawnPoint> SpawnPoints = new List<SpawnPoint>();
        [SerializeField] private List<EnemyController> Enemies = new List<EnemyController>();
        [SerializeField] private int _maxSpawnEnemies;
        private List<UnitStats> _currentEnemiesSpawned = new List<UnitStats>();
        
        private void OnDisable()
        {
            StopAllCoroutines();
        }

        public void SpawnEnemies()
        {
            StartCoroutine(SpawnCycle());
        }

        public void DespawnAllEnemies()
        {
            StopAllCoroutines();
            var temp = new List<UnitStats>();
            foreach (var enemy in _currentEnemiesSpawned)
            {
                temp.Add(enemy);
            }

            foreach (var enemy in temp)
            {
                DestroyUnit(enemy);
            }
        }

        private IEnumerator SpawnCycle()
        {
            while (true)
            {
                while (_currentEnemiesSpawned.Count < _maxSpawnEnemies)
                {
                    SpawnRandomUnit();
                }
                yield return new WaitForSeconds(1f);
            }
        }

        private void SpawnRandomUnit()
        {
            var enemy = Instantiate(Enemies[Random.Range(0, Enemies.Count)], this.transform);
            var randomPosition = SpawnPoints[Random.Range(0, SpawnPoints.Count)];
            enemy.transform.position = randomPosition.transform.position;
            enemy.Stats.OnDeath += DestroyUnit;
            _currentEnemiesSpawned.Add(enemy.Stats);
        }

        private void DestroyUnit(UnitStats sender)
        {
            OnDespawnEnemy?.Invoke();
            _currentEnemiesSpawned.Remove(sender);
            sender.OnDeath -= DestroyUnit;
            Destroy(sender.gameObject);
        }
    }
}