﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Scripts.Gameplay.CoreGameplay.Radar
{
    public class TargetsRadar : MonoBehaviour
    {
        [field: SerializeField] public List<GameObject> TargetsInRadius { get; private set; }
        public event Action<GameObject> OnTargetEnter;
        public event Action<GameObject> OnTargetExit;

        [SerializeField, Range(0.02f, 2f)] 
        private float _periodCheckNullObject = 0.2f;
        private SphereCollider _collider;

        private void Awake()
        {
            _collider = GetComponent<SphereCollider>();
            _collider.isTrigger = true;
            TargetsInRadius = new List<GameObject>();
        }
        
        public void SetRadius(float radius)
        {
            _collider.radius = radius;
        }
        
        public GameObject GetNearestTarget()
        {
            if (TargetsInRadius.Count > 0)
            {
                var nearIndex = 0;
                var minDistance = float.MaxValue;

                for (int i = 0; i < TargetsInRadius.Count; i++)
                {
                    var distance = transform.position - TargetsInRadius[i].transform.position;
                    float sqrLen = distance.sqrMagnitude;

                    if (sqrLen < minDistance)
                    {
                        minDistance = sqrLen;
                        nearIndex = i;
                    }
                }
                return TargetsInRadius[nearIndex];
            }
            return null;
        }

        private void OnEnable()
        {
            StartCoroutine(CheckNullObject());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        private IEnumerator CheckNullObject()
        {
            while (true)
            {
                TargetsInRadius.RemoveAll(target => target == null);
                yield return new WaitForSeconds(_periodCheckNullObject);
            }
        }

        public void OnTriggerEnter(Collider other)
        {
            GameObject triggerObject = other.attachedRigidbody != null 
                ? other.attachedRigidbody.gameObject : other.gameObject;

            if (triggerObject != null && !TargetsInRadius.Contains(triggerObject))
            {
                TargetsInRadius.Add(triggerObject);
                OnTargetEnter?.Invoke(triggerObject);
            }
        }

        public void OnTriggerExit(Collider other)
        {
            GameObject triggerObject = other.attachedRigidbody != null 
                ? other.attachedRigidbody.gameObject : other.gameObject;

            if (triggerObject != null && TargetsInRadius.Contains(triggerObject))
            {
                TargetsInRadius.Remove(triggerObject);
                OnTargetExit?.Invoke(triggerObject);
            }
        }
    }
}